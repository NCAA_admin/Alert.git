﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyEvent : MonoBehaviour
{
    /// <summary>
    /// 获取Alert组件
    /// </summary>
    private Alert alert;
    void Start()
    {
        //实例化组件
        alert = Alert.Instance;
    }

    void Update()
    {
        //键盘事件
        _KeyDown();
    }

    /// <summary>
    /// 按下键盘执行
    /// </summary>
    public void _KeyDown()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            alert._SetShow();
        }
    }

}
