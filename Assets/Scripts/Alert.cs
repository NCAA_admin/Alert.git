﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.EventSystems;
//显示Alert会话
public class Alert : MonoBehaviour
{
    public static Alert Instance;
    private void Awake()
    {
        Instance = this;
    }
    [Tooltip("会话对象")] public RectTransform m_alert;
    //获取ui blur(alert)初始大小
    private Vector2 _uiBlurSize;
    //获取ui blur(alert)初始scale
    private Vector2 _uiBlurScale;
    //透明遮罩层
    private RectTransform _transparentMask;
    //透明遮罩层的初始颜色值值
    private Color _transparentMaskColor;
    //获取confirm
    private RectTransform _confirm;
    //获取confirm初始大小
    private Vector2 _confirmSize;
    //获取confirm初始scale
    private Vector2 _confirmScale;
    //获取text对象
    private Text _text;
    //获取text的大小
    private Vector2 _textSize;
    //获取text scale
    private Vector2 _textScale;
    void Start()
    {
        //判断并且赋值
        if (!m_alert)
        {
            m_alert = transform.GetComponent<RectTransform>();
        }
        //获取uiblur(alert)初始大小
        _uiBlurSize = m_alert.rect.size;
        _uiBlurScale = m_alert.localScale;
        //获取透明遮罩层
        _transparentMask = m_alert.GetChild(0).GetComponent<RectTransform>();
        //透明遮罩层的颜色
        _transparentMaskColor = _transparentMask.GetComponent<Image>().color;
        //获取confirm
        _confirm = m_alert.GetChild(1).GetComponent<RectTransform>();
        //获取confirm初始大小
        _confirmSize = _confirm.rect.size;
        _confirmScale = _confirm.localScale;
        //获取text对象
        _text = _confirm.GetChild(0).GetComponent<Text>();
        //获取text的大小
        _textSize = _text.GetComponent<RectTransform>().rect.size;
        _textScale = _text.rectTransform.localScale;
        //确定取消按钮
        _Confirm();
        _Cancel();
        //初始设置隐藏
        if (m_hideOnStart)
            _SetHideImmediate();
    }

    /// <summary>
    /// 无动画立即隐藏
    /// </summary>
    public void _SetHideImmediate()
    {
        //设置隐藏
        m_alert.gameObject.SetActive(false);
        _confirm.gameObject.SetActive(false);
        _text.gameObject.SetActive(false);
        _transparentMask.gameObject.SetActive(false);
        //宽高设置0
        m_alert.DOScale(Vector3.zero, 0);
        _confirm.DOScale(Vector3.zero, 0);
        //设置遮罩层alpha = 0
        _transparentMask.GetComponent<Image>().DOFade(0, 0);
    }
    /// <summary>
    /// 开始时为隐藏状态
    /// </summary>
    [Tooltip("开始时为隐藏状态")]
    public bool m_hideOnStart = true;
    /// <summary>
    /// 隐藏
    /// </summary>
    public void _SetHide()
    {
        //设置隐藏
        _text.gameObject.SetActive(false);
        //宽高设置0
        m_alert.DOScale(Vector2.zero, _SetShow_duration)
            .onComplete = () =>
            {
                m_alert.gameObject.SetActive(false);
            };
        _confirm.DOScale(Vector2.zero, _SetShow_duration)
            .onComplete = () =>
            {
                _confirm.gameObject.SetActive(false);
            };
        //隐藏透明遮罩层
        _transparentMask.GetComponent<Image>().DOFade(0, _SetShow_duration)
            .onComplete = () =>
            {
                _transparentMask.gameObject.SetActive(false);
            };
    }
    /// <summary>
    /// 显示
    /// </summary>
    public void _SetShow()
    {
        //设置显示
        m_alert.gameObject.SetActive(true);
        _confirm.gameObject.SetActive(true);
        _transparentMask.gameObject.SetActive(true);
        //宽高设置成初始大小+5%再回到原来的大小
        m_alert.DOScale(_uiBlurScale + _uiBlurScale * 0.05f, _SetShow_duration)
            .onComplete = () =>
            {
                //完成后回到初始大小 时间为原来的1/5
                m_alert.DOScale(_uiBlurScale, _SetShow_duration / 5f);
            };
        _confirm.DOScale(_confirmScale + _confirmScale * 0.05f, _SetShow_duration)
            .onComplete = () =>
            {
                //完成后回到初始大小 时间为原来的1/5
                _confirm.DOScale(_confirmScale, _SetShow_duration / 5f);
                //并且显示text
                _text.gameObject.SetActive(true);
            };
        //显示透明遮罩层
        _transparentMask.GetComponent<Image>().DOFade(_transparentMaskColor.a, _SetShow_duration);
    }
    /// <summary>
    /// 显示动画时间
    /// </summary>
    private float _SetShow_duration = 0.3f;

    /// <summary>
    /// 按钮的hover效果
    /// </summary>
    /// <param name="button">按钮</param>
    public void _OnHover(Transform button)
    {
        if (!m_inited)
        {
            m_inited = true;
            //获取初始大小
            m_initialScale = transform.GetComponent<RectTransform>().localScale.x;
        }
        if (!button.GetComponent<EventTrigger>()) button.gameObject.AddComponent<EventTrigger>();
        EventTrigger trigger = button.GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerEnter;
        entry.callback = new EventTrigger.TriggerEvent();
        entry.callback.AddListener(delegate (BaseEventData baseEvent) {
            button.GetComponent<RectTransform>().DOScale(m_localScale, 0.3f);
        });
        trigger.triggers.Add(entry);

        EventTrigger.Entry entry2 = new EventTrigger.Entry();
        entry2.eventID = EventTriggerType.PointerExit;
        entry2.callback = new EventTrigger.TriggerEvent();
        entry2.callback.AddListener(delegate (BaseEventData baseEvent) {
            button.GetComponent<RectTransform>().DOScale(m_initialScale, 0.3f);
        });
        trigger.triggers.Add(entry2);
    }
    /// <summary>
    /// 变大尺度默认1
    /// </summary>
    [Tooltip("变大尺度默认1.2f")]
    public float m_localScale = 1.2f;
    /// <summary>
    /// 初始的大小
    /// </summary>
    private float m_initialScale;
    /// <summary>
    /// 是否被初始化
    /// </summary>
    private bool m_inited = false;

    /// <summary>
    /// 确定
    /// </summary>
    public void _Confirm()
    {
        _OnHover(m_confirm);
        m_confirm.GetComponent<Button>().onClick.AddListener(delegate {
            _SetHide();
        });
    }
    /// <summary>
    /// 确定按钮
    /// </summary>
    [Tooltip("确定按钮")]
    public Transform m_confirm;
    /// <summary>
    /// 取消按钮
    /// </summary>
    public void _Cancel()
    {
        _OnHover(m_cancel);
        m_cancel.GetComponent<Button>().onClick.AddListener(delegate {
            _SetHide();
        });
    }
    [Tooltip("取消按钮")]
    public Transform m_cancel;

    

}
